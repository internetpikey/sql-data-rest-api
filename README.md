# SQL Instant Rest API

## Installation

Copy all files to your server and run `composer install` from the root directory. 

Rename `src/settings.example.php` to `src/settings.php` and populate the database configuration.

Next, set up your vhosts configuration to point to the `public` folder.

## Using the API

### Retrieving records - `/{TABLE_NAME}/list`

When retrieving records each parameter becomes a filter e.g. `/{TABLE_NAME}/list?foo=hello` would retrieve all records containing "hello" in the `foo` column.

#### Range Filters

In addition to being able to search by exact matches you can define minimum and maximum values for any numeric field. If your table contains a column called date you could for example search `/{TABLE_NAME}/list?min_date=2016-01-01&max_date=2016-01-31` to retrieve all records with a date between the 1st and 31st of January 2016.

### Adding a single row - `/{TABLE_NAME}/add`

Single rows are added with a form post containing one parameter per colums e.g. `/{TABLE_NAME}/add?foo=hello&bar=world` would create a new record with the column `foo` set to "hello" and `bar` set to "world".

### Deleting rows - `/{TABLE_NAME}/delete`

Delete rows with a request containing the filters e.g. `/{TABLE_NAME}/delete?foo=hello` will delete all rows with the column `foo` containing "hello".

### Inserting bulk data - `/{TABLE_NAME}/batch`

Bulk data is imported by posting raw JSON data to `/{TABLE_NAME}/batch`. For example:

```json
{
  "data": [
    {
      "foo": "hello",
      "bar": "world",
      "date": "2016-05-11"
    },
    {
      "foo": "goodbye",
      "bar": "mars",
      "date": "2016-05-11"
    }
  ],
  "update": [
    "bar"
  ]
}
```

The above example would create 2 new records, however if foo where a unique field and and a record containing it already existsted it would only update the date column to the new value.

## ToDo

- Proper namespaced structure using services for helper functions.