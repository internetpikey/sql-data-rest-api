<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'itemsPerPage' => 100,
        
        // Database settings
        'db' => [
            "host" => "127.0.0.1",
            "username" => "root",
            "password" => "",
            "database" => "database",
            "port" => ""
        ],

        // Renderer settings
        'renderer' => [
            'template_path' => APP_ROOT . '/templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'sql-rest-api',
            'path' => APP_ROOT . '/logs/app.log',
        ],
    ],
];