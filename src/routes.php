<?php
// Routes

$app->get('/{table}/delete', function ($request, $response, $args) {
    $this->db->beginTransaction();
    try {
        $query = $this->db->query('DELETE' . buildQuery($this->db, $args['table'], $_REQUEST));
        $output = array(
            'status' => 'success',
            'message' => 'Records deleted.',
        );
    } catch (PDOException $e){
        // write error to log
        $this->logger->error($e->getMessage());
        
        // output error
        $output = array(
            'status' => 'error',
            'message' => $e->getMessage(),
        );
    }
    $this->db->commit();
    
    return $response->write(json_encode($output, JSON_UNESCAPED_SLASHES));
});

function buildQuery($db, $table, $params) {
    // get column names
    $columns = $db->query("DESCRIBE $table")->fetchAll(PDO::FETCH_COLUMN);
    
    // get filter vars & build query
    $query = array();
    foreach ($params as $param => $value) {
        $mod = substr($param, 0, 3);
        if($mod == 'min' || $mod == 'max') {
            $param = substr($param, 4);
        }
        
        if(in_array($param, $columns)) {
            switch ($mod) {
                case 'min':
                    $query[] = "`$param` >= '$value' ";
                    break;
                case 'max':
                    $query[] = "`$param` <= '$value'";
                    break;
                default:
                    $query[] = "`$param` = '$value'";
                    break;
            }
        }
    }
    $filter = implode($query, ' AND ');
    $sql = " FROM `$table`" . ($filter ? ' WHERE ' . $filter : '');
    return $sql;
}

$app->get('/{table}/list[/{page}]', function ($request, $response, $args) {
    $sql = buildQuery($this->db, $args['table'], $_REQUEST);
    // calculate total pages
    $total = $this->db->query("SELECT COUNT(id) $sql")->fetchColumn();
    $limit = $this->get('settings')['itemsPerPage'];
    $pages = ceil($total / $limit);
    
    $current = (isset($args['page']) ? $args['page'] : 1);
    $page = filter_var($current, FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => $pages)));
    if($current > $pages) {
        return $response->write(json_encode(array('status'=>'error','message'=>'Page doesn\'t exist.'), JSON_UNESCAPED_SLASHES));
        
    }
    $start = $limit * ($page - 1);
    
    // get data
    $data = $this->db->query("SELECT * $sql LIMIT $start, $limit")->fetchAll();
    
    // build paging cursors
    $uri = $request->getUri();
    $baseUri = $uri->getScheme() . '://' . $uri->getHost() . $uri->getBasePath() . '/' . $args['table'] . '/list/';
    $cursor = array();
    if($page > 1) {
        $cursor['previous'] = $baseUri . ($page - 1) . '?' . $uri->getQuery();
    }
    if($page < $pages) {
        $cursor['next'] = $baseUri . ($page + 1) . '?' . $uri->getQuery();
    }
    
    $output = array(
        'paging' => array(
            'records' => filter_var($total, FILTER_VALIDATE_INT),
            'pages' => $pages,
            'cursor' => $cursor,
        ),
        'data' => $data,
    );
    
    return $response->write(json_encode($output, JSON_UNESCAPED_SLASHES));
});

$app->get('/{table}/add', function ($request, $response, $args) {
    $table = $args['table'];
    
    // get column names
    $columns = $this->db->query("DESCRIBE `$table`")->fetchAll(PDO::FETCH_COLUMN);
    $data = array();
    foreach ($_REQUEST as $param => $value) {
        if(in_array($param, $columns)) {
            $data[$param] = $value;
        }
    }
    
    $sql = "INSERT INTO `$table` (" . implode(',', array_keys($data)) . ') VALUES (' . str_repeat("?,", count($data)-1) . '?)';

    $this->db->beginTransaction();  // helps speed up inserts.
    try {
        $query = $this->db->prepare($sql);
        $query->execute(array_values($data));
    } catch (PDOException $e){
        // write error to log
        $this->logger->error($e->getMessage());
        
        // output error
        $output = array(
            'status' => 'error',
            'message' => $e->getMessage(),
        );
        return $response->write(json_encode($output, JSON_UNESCAPED_SLASHES));
    }
    $this->db->commit();
});

function ksortRecursive(&$array, $sort_flags = SORT_REGULAR) {
    if (!is_array($array)) return false;
    ksort($array, $sort_flags);
    foreach ($array as &$arr) {
        ksortRecursive($arr, $sort_flags);
    }
    return true;
}

function placeholders($text, $count=0, $separator=','){
    $result = array();
    if($count > 0){
        for($x=0; $x<$count; $x++){
            $result[] = $text;
        }
    }

    return implode($separator, $result);
}

$app->post('/{table}/batch', function ($request, $response, $args) {
    
    // get column names
    $columns = $this->db->query('DESCRIBE `' . $args['table'] . '`')->fetchAll(PDO::FETCH_COLUMN);
    
    // parse json post data
    $post = json_decode(file_get_contents('php://input'));
    
    $data = array();
    $active_columns = array();
    
    if(isset($post->data)) {
        // filter data (by columns existing in db) and set active columns
        foreach ($post->data as $id => $row) {
            foreach ($columns as $column) {
                if(isset($row->$column)) {
                    $data[$id][$column] = $row->$column;
                    $active_columns[$column] = 1;
                }
            }
        }
        
        // add empty cells to maintain equal columns for prepared statement
        foreach ($data as $id => $row) {
            foreach ($active_columns as $column => $b) {
                if(isset($active_columns[$column]) && !isset($row[$column])) {
                    $data[$id][$column] = null;
                }
            }
        }
        
        // sort columns to make sure they are in right order for prepared statement
        ksortRecursive($data);
        ksort($active_columns);
        
        $insert_values = array();
        foreach($data as $d){
            $question_marks[] = '('  . placeholders('?', sizeof($d)) . ')';
            $insert_values = array_merge($insert_values, array_values($d));
        }
        
        $sql = 'INSERT INTO ' . $args['table'] . ' (' . implode(',', array_keys($active_columns) ) . ') VALUES ' . implode(',', $question_marks);
        
        // update statement
        if(isset($post->update)) {
            foreach ($post->update as $field) {
                $update[] = "$field = VALUES($field)";
            }
            $sql .= ' ON DUPLICATE KEY UPDATE ' . implode(', ', $update);
        }
        
        $this->db->beginTransaction();  // helps speed up inserts.
        try {
            $query = $this->db->prepare ($sql);
            $query->execute($insert_values);
        } catch (PDOException $e){
            // write error to log
            $this->logger->error($e->getMessage());
            
            // output error
            $output = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            return $response->write(json_encode($output, JSON_UNESCAPED_SLASHES));
        }
        $this->db->commit();
        
        $output = array(
            'status' => 'success',
            'message' => 'Successfully updated table ' . $args['table'],
        );
        return $response->write(json_encode($output, JSON_UNESCAPED_SLASHES));
    }
});

/*
$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info('Slim-Skeleton '/' route');

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
*/