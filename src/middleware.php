<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

// Authentication
$app->add(new \Slim\Middleware\HttpBasicAuthentication([
    "path" => "/",
    "realm" => "Protected",
    "secure" => false,
    "error" => function ($request, $response, $arguments) {
        $data = [];
        $data["status"] = "error";
        $data["message"] = $arguments["message"];
        return $response->write(json_encode($data, JSON_UNESCAPED_SLASHES));
    },
    "authenticator" => function ($arguments) {
        $api_key = (isset($_REQUEST['api_key']) ? $_REQUEST['api_key'] : false);
        if(!$api_key) {
            // to do: add basic auth option
            return false;
        }
        $handle = fopen(APP_ROOT . "/keys.txt", "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if(trim($line) == $_REQUEST['api_key']) {
                    $authorised = true;
                    break;
                }
            }
            fclose($handle);
            return (isset($authorised) && $authorised);
        } else {
            die("Error reading api keys. Please make sure you have a file named keys.txt in the same directory as this script.");
        }
    }
]));